
var request = require('request');

module.exports = {
    get: function (game_state, fn) {
         var myCards = game_state.players[game_state.in_action].hole_cards;
         var allCards = myCards.concat(game_state.community_cards);

        // console.log("all: ");
        // console.log(JSON.stringify(allCards));
        var propertiesObject = { cards: JSON.stringify(allCards) };

        request({ url: 'http://rainman.leanpoker.org/rank', qs: propertiesObject }, function (err, response, body) {
            if (err) { console.log(err); return; }
            
             console.log("Get response: " + response.statusCode);
            // console.log(response.request.url);
            var result = JSON.parse(response.body);
            // console.log(response.body);

            fn(result);
        });

    }

};