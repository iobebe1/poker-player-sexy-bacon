var _ = require('underscore');
var ranking = require("./ranking");

module.exports = {

  VERSION: "V14",

  bet_request: function(game_state, bet) {
    try{       
        if (game_state.community_cards && game_state.community_cards.length == 0){
          //BEFORE FLOP (0)
          console.log("("+game_state.game_id+", "+game_state.round+", "+game_state.bet_index+", "+game_state.orbits+"): before flop");

          return bet(this.beforeTurn(game_state));          
        }
        else if (game_state.community_cards && game_state.community_cards.length <5){
          //BEFORE RIVER (3 & 4)
          console.log("("+game_state.game_id+", "+game_state.round+", "+game_state.bet_index+", "+game_state.orbits+"): before river");

          return bet(this.afterTurn(game_state));          
        }
        else{
          //ON RIVER
          console.log("("+game_state.game_id+", "+game_state.round+", "+game_state.bet_index+", "+game_state.orbits+"): on river");
          //return bet(this.beforeTurn(game_state));          
          //return bet(this.afterTurn(game_state));          
          //return bet(this.atEnd(game_state));     

          var leRaise = this.raise(game_state);
          ranking.get(game_state, function(o)
          {
            if(o.rank >= 2 || (o.rank==1 && o.value > 9))
            {
              console.log("atEnd: rank: "+o.rank+" value: "+o.value);
              console.log("atEnd - rasied: ".concat(leRaise))
              bet(leRaise);
            }
            else{
              console.log("atEnd - fold: ");
              bet(0);  
            }
            
          });
	       
        }   
    }
    catch (err){
      console.log(err);      
      bet(1000);
    }
    finally{
    }    
  },

  showdown: function(game_state) {

  },

  beforeTurn : function (game_state){
    var myCards = game_state.players[game_state.in_action].hole_cards;
    if(this.color(myCards) || this.pair(myCards) || this.over10(myCards)){
      var leRaise = this.raise(game_state);
       if(leRaise > 300 && (!this.pair(myCards)))
      {
          return 0;
      }
      console.log("rasied: ".concat(leRaise))
      return leRaise;
    }
    else{
      // var myLastBet = game_state.players[game_state.in_action].bet;
      // if (game_state.bet_index == 0){
      //   var leRaise = this.raise(game_state) + 1;
      //   console.log("rasied (but crap cards): ".concat(leRaise))
      //   return leRaise;
      // }
      return 0;
    }
  },

  raise : function (game_state){
    //current_buy_in - players[in_action][bet] + minimum_raise
    //return game_state.current_buy_in + 10 * 2 *game_state.small_blind
    return game_state.current_buy_in - game_state.players[game_state.in_action].bet + game_state.minimum_raise + 105;
  },

  color: function(myCards){
      return myCards[0].suit === myCards[1].suit;
  },

  pair: function(myCards){  
      return myCards[0].rank === myCards[1].rank;    
  },

  over10: function(myCards){
    return (myCards[0].rank === "10" || myCards[0].rank === "J" || myCards[0].rank === "Q" || myCards[0].rank === "K" ||myCards[0].rank === "A") 
      || (myCards[1].rank === "10" || myCards[1].rank === "J" || myCards[1].rank === "Q" || myCards[1].rank === "K" ||myCards[1].rank === "A")
  },

  //after turn
  afterTurn : function (game_state){
    var myCards = game_state.players[game_state.in_action].hole_cards;
    var allCards = myCards.concat(game_state.community_cards);
    allCards = this.toAlpha(allCards);

    if(this.atLeast4OfSameColor(game_state) || this.onePair(allCards)){
      var leRaise = this.raise(game_state);
      console.log("rasied: ".concat(leRaise))
      return leRaise;
    }
    else{
      return 0;
    }
  },

  toAlpha : function(myCards){
    var asAlpha = _.map(myCards, function(x){
      if (x.rank=="A")
        return {
          rank : "14",
          suit : x.suit,
        };
      if (x.rank=="K")
        return {
          rank : "13",
          suit : x.suit,
        };
      if (x.rank=="Q")
        return {
          rank : "12",
          suit : x.suit,
        };
      if (x.rank=="J")
        return {
          rank : "11",
          suit : x.suit,
        };
      if (x.rank=="10")
        return x;
      
      return {
          rank : "0"+x.rank,
          suit : x.suit,
        };      
    });

    // asAlpha.forEach(function(x) {
    //   console.log("log - asAlpha - list: "+JSON.stringify(x));
    // }, this);

    return asAlpha
  },

  onePair: function(myCards){
    try{
      var groups = _.countBy(myCards, function(x){
        return x.rank;
      });

      var ordered = _.sortBy(groups, function(x){
        return x;
      }).reverse();

      // ordered.forEach(function(x) {
      //   console.log("log - pair2 - list: "+JSON.stringify(x));
      // }, this);    
         
      console.log("log - pair2: ".concat(ordered[0] > 1));
      return ordered[0] > 1;
    }
    catch(err){
      console.log("error: pair2 function failed");
      console.log(err);
      return pair(myCards);
    }
  },

  atLeast4OfSameColor: function(game_state){
    try{
      var myCards = game_state.players[game_state.in_action].hole_cards;
      var allCards = myCards.concat(game_state.community_cards);

      var groups = _.groupBy(allCards, function(x){
        return x.suit;
      });

      var ordered = _.sortBy(groups, function(x){
        return x.length;
      }).reverse();      
      
      var topColorCards = ordered[0];
      console.log("log - atLeast4OfSameColor: "+topColorCards.length+" of same color");

      var myCards = game_state.players[game_state.in_action].hole_cards;      
      var colorsInMyHand = _.intersection(topColorCards, myCards);

      console.log("log - atLeast4OfSameColor: "+colorsInMyHand.length+" in my hand");
             
      var goOn = topColorCards.length > 3 && colorsInMyHand.length >=1;
      console.log("log - atLeast4OfSameColor: "+goOn)
      return goOn;
    }
    catch(err){
      console.log("error: pair2 function failed");
      console.log(err);
      return color(myCards);
    }
  },

  maybeStraight : function(allCards){    
      var ordered = _.sortBy(allCards, function(x){
        return x.rank;
      });

      var gapOf2 = 0;      
      var gapsOver2 = 0;      
      for (i = 1; i < ordered.length; i=i+2) { 
          var prev = ordered[i-1].rank;
          var curr = ordered[i].rank;

          if (curr-prev == 2){
            gapOf2 = gapOf2 +1;
          }
          if (curr-prev > 2){
            gapsOver2 = gapsOver2 +1;
          }
      }

      var goOn = gapsOver2 == 0 && gapsOf2 <=1;


      if (ordered.length == 5){
          console.log("log - maybeStraight - 5 cards: "+gaps+" gaps");
          var res = gaps > 1;
          console.log("log - maybeStraight: "+res);
          return res;
      }
      else{
          console.log("log - maybeStraight - 6 cards: "+gaps+" gaps");
          var res = gaps > 1;
          console.log("log - maybeStraight: "+res);
          return res;
      }
            
  },


  //AT END
  atEnd : function (game_state){
    var myCards = game_state.players[game_state.in_action].hole_cards;
    if(this.color(myCards) || this.pair(myCards) || this.over10(myCards)){
      var leRaise = this.raise(game_state);
      console.log("rasied: ".concat(leRaise))
      return leRaise;
    }
    else{
      return 0;
    }
  },
  
};
